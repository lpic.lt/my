# This file is a template, and might need editing before it works on your project.
FROM con.vgtu.lt/debian/debian:testing-min
LABEL maintainer="Ruslanas <ruslanas@lpic.lt>"
RUN apt install --no-install-recommends -y nginx-light ; apt clean
COPY ./*html /var/www/html/
EXPOSE 80
STOPSIGNAL SIGTERM
CMD ["nginx", "-g", "daemon off;"]
